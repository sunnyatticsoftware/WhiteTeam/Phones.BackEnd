﻿using Phones.BackEnd.Application.Contracts.Factories;
using Phones.BackEnd.Application.Contracts.Repositories;
using Phones.BackEnd.Application.Contracts.Services;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.Domain;
using System;
using System.Threading.Tasks;

namespace Phones.BackEnd.Application.Services
{
    public class PhoneWriteService
        : IPhoneWriteService
    {
        private readonly IRepository<PhoneAggregate> _repository;
        private readonly IIdFactory _idFactory;

        public PhoneWriteService(
            IRepository<PhoneAggregate> repository,
            IIdFactory idFactory)
        {
            _repository = repository;
            _idFactory = idFactory;
        }

        public async Task CreatePhone(PhoneNew phone)
        {
            var id = _idFactory.Create();
            var brandType = phone.Brand;
            var model = phone.Model;
            var description = phone.Description;
            phone.Id = id;

            var phoneAggregate = new PhoneAggregate(id, brandType, model, description);

            await _repository.Persist(phoneAggregate);
        }

        public async Task AddImage(Guid phoneId, Uri imageUri)
        {
            var phoneAggregate = await _repository.Get(phoneId);
            phoneAggregate.AddImageUri(imageUri);

            await _repository.Persist(phoneAggregate);
        }
    }
}
