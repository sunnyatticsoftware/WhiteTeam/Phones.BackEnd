﻿using Phones.BackEnd.Application.Contracts;
using Phones.BackEnd.Application.Contracts.Repositories;
using Phones.BackEnd.Application.Contracts.Services;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phones.BackEnd.Application.Services
{
    public class PhoneReadService
        : IPhoneReadService
    {
        private readonly IRepository<PhoneAggregate> _repository;
        private readonly IMapper<PhoneAggregate, PhoneResponse> _phoneResponseMapper;

        public PhoneReadService(
            IRepository<PhoneAggregate> repository,
            IMapper<PhoneAggregate, PhoneResponse> phoneResponseMapper)
        {
            _repository = repository;
            _phoneResponseMapper = phoneResponseMapper;
        }

        public async Task<IEnumerable<PhoneResponse>> GetPhones()
        {
            var phones = await _repository.GetAll();
            var phoneResponses = phones.Select(x => _phoneResponseMapper.Map(x));
            return phoneResponses;
        }

        public async Task<PhoneResponse> GetPhone(Guid id)
        {
            var phone = await _repository.Get(id);
            if (phone is null)
            {
                throw new KeyNotFoundException($"Could not find phone with Id {id}");
            }

            var phoneResponse = _phoneResponseMapper.Map(phone);
            return phoneResponse;
        }
    }
}