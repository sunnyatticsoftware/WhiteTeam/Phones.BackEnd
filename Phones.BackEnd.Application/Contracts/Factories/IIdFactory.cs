﻿using System;

namespace Phones.BackEnd.Application.Contracts.Factories
{
    public interface IIdFactory
    {
        Guid Create();
    }
}