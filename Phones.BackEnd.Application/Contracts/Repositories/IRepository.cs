﻿using Phones.BackEnd.Domain.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phones.BackEnd.Application.Contracts.Repositories
{
    public interface IRepository<TAggregate>
        where TAggregate : IAggregate
    {
        Task Persist(TAggregate aggregate);
        Task<TAggregate> Get(Guid id);
        Task<IEnumerable<TAggregate>> GetAll();
    }
}