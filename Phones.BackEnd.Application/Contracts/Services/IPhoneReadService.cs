﻿using Phones.BackEnd.Application.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phones.BackEnd.Application.Contracts.Services
{
    public interface IPhoneReadService
    {
        Task<IEnumerable<PhoneResponse>> GetPhones();
        Task<PhoneResponse> GetPhone(Guid id);
    }
}