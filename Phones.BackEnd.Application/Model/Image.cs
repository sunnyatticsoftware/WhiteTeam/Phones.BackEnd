﻿using System;

namespace Phones.BackEnd.Application.Model
{
    public class Image
    {
        public Uri Uri { get; set; }
    }
}