﻿using Phones.BackEnd.Types;
using System;

namespace Phones.BackEnd.Application.Model
{
    public class PhoneNew
    {
        public Guid? Id { get; set; }
        public BrandType Brand { get; set; }
        public string Model { get; set; }
        public string Description { get; set; }
    }
}
