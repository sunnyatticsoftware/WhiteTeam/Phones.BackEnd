﻿using Phones.BackEnd.Application.Contracts.Factories;
using System;

namespace Phones.BackEnd.Application.Factories
{
    public class IdFactory
        : IIdFactory
    {
        public Guid Create()
        {
            var id = Guid.NewGuid();
            return id;
        }
    }
}