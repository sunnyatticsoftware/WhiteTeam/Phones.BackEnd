﻿using Phones.BackEnd.Application.Contracts;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.Domain;

namespace Phones.BackEnd.Application.Mappers
{
    public class PhoneResponseMapper
        : IMapper<PhoneAggregate, PhoneResponse>
    {
        public PhoneResponse Map(PhoneAggregate source)
        {
            var id = source.Id;
            var title = $"{source.Brand} {source.Model}";
            var description = source.Description;
            var imageUri = source.ImageUri;
            var phoneResponse =
                new PhoneResponse
                {
                    Id = id,
                    Title = title,
                    Description = description,
                    ImageUri = imageUri
                };
            return phoneResponse;
        }
    }
}
