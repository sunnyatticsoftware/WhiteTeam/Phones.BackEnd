﻿using FluentAssertions;
using Newtonsoft.Json;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.Types;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Phones.BackEnd.ComponentTests.Features.GetPhones
{
    [Binding]
    [Scope(Feature = "Get Phones")]
    public class CreatePhoneSteps
        : ComponentTestFeature
    {
        private HttpResponseMessage _result;

        [Given(@"there are (.*) phones in the system")]
        public async Task GivenThereArePhonesInTheSystem(int p0)
        {
            for (int i = 0; i < p0; i++)
            {
                var phone =
                    new PhoneNew
                    {
                        Brand = BrandType.Motorola,
                        Model = $"Model{i}",
                        Description = $"Description{i}"
                    };
                var serializedPhoneNew = JsonConvert.SerializeObject(phone);
                var stringContent = new StringContent(serializedPhoneNew, Encoding.UTF8, "application/json");
                await TestHttpClient.PostAsync("api/phones", stringContent);
            }

        }

        [When(@"I get the phones")]
        public async Task WhenIGetThePhones()
        {
            _result = await TestHttpClient.GetAsync("api/phones");
        }

        [Then(@"a list of (.*) phones is displayed")]
        public async Task ThenAListOfPhonesIsDisplayed(int p0)
        {
            var serializedResponse = await _result.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<IEnumerable<PhoneResponse>>(serializedResponse);
            response.Count().Should().Be(p0);
        }
    }
}
