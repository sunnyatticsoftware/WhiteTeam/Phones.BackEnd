﻿Feature: Add Image
	As a candidate evaluator
	I want to add an image to an existing phone
	So that I can verify Diego knows what's doing when running write operations on existing items

@phone
Scenario: Adds image to a phone
	Given there is a phone in the system
	And there is a valid image uri "https://pressdispensary.co.uk/ext/1592/images/gtext%20media%20-%20phone%20sample.jpg"
	When I add the image uri to the phone
	Then the phone is updated
	And its image uri is "https://pressdispensary.co.uk/ext/1592/images/gtext%20media%20-%20phone%20sample.jpg"