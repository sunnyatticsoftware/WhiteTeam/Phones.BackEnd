﻿using FluentAssertions;
using Newtonsoft.Json;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.Types;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Phones.BackEnd.ComponentTests.Features.AddImage
{
    [Binding]
    [Scope(Feature = "Add Image")]
    public class AddImageSteps
        : ComponentTestFeature
    {
        private Uri _uri;
        private HttpResponseMessage _result;
        private Guid _phoneId;

        [Given(@"there is a phone in the system")]
        public async Task GivenThereIsAPhoneInTheSystem()
        {
            var phone =
                new PhoneNew
                {
                    Brand = BrandType.Motorola,
                    Model = "N600",
                    Description = "The best budget phone"
                };
            var serializedPhoneNew = JsonConvert.SerializeObject(phone);
            var stringContent = new StringContent(serializedPhoneNew, Encoding.UTF8, "application/json");
            var response = await TestHttpClient.PostAsync("api/phones", stringContent);
            var serializedResponse = await response.Content.ReadAsStringAsync();
            var phoneCreated = JsonConvert.DeserializeObject<PhoneResponse>(serializedResponse);
            _phoneId = phoneCreated.Id;
        }

        [Given(@"there is a valid image uri ""(.*)""")]
        public void GivenThereIsAValidImageUri(string p0)
        {
            _uri = new Uri(p0);
        }

        [When(@"I add the image uri to the phone")]
        public async Task WhenIAddTheImageUriToThePhone()
        {
            var image =
                new Image
                {
                    Uri = _uri
                };
            var serializedImage = JsonConvert.SerializeObject(image);
            var stringContent = new StringContent(serializedImage, Encoding.UTF8, "application/json");
            _result = await TestHttpClient.PutAsync($"api/phones/{_phoneId}/image", stringContent);
        }

        [Then(@"the phone is updated")]
        public void ThenThePhoneIsUpdated()
        {
            _result.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Then(@"its image uri is ""(.*)""")]
        public async Task ThenItsImageUriIs(string p0)
        {
            var response = await TestHttpClient.GetAsync($"api/phones/{_phoneId}");
            var serializedResponse = await response.Content.ReadAsStringAsync();
            var phone = JsonConvert.DeserializeObject<PhoneResponse>(serializedResponse);
            phone.ImageUri.Should().Be(p0);
        }
    }
}
