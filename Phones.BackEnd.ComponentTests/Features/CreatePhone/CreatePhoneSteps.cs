﻿using FluentAssertions;
using Newtonsoft.Json;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.Types;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Phones.BackEnd.ComponentTests.Features.CreatePhone
{
    [Binding]
    [Scope(Feature = "Create Phone")]
    public class CreatePhoneSteps
        : ComponentTestFeature
    {
        private PhoneNew _phone;
        private HttpResponseMessage _response;

        [Given(@"there are no phones in the system")]
        public void GivenThereAreNoPhonesInTheSystem()
        {
        }

        [Given(@"there is a phone")]
        public void GivenThereIsAPhone()
        {
            _phone = new PhoneNew();
        }

        [Given(@"its brand is ""(.*)""")]
        public void GivenItsBrandIs(int p0)
        {
            _phone.Brand = (BrandType)p0;
        }

        [Given(@"its model is ""(.*)""")]
        public void GivenItsModelIs(string p0)
        {
            _phone.Model = p0;
        }

        [Given(@"its description is ""(.*)""")]
        public void GivenItsDescriptionIs(string p0)
        {
            _phone.Description = p0;
        }

        [When(@"I add the phone")]
        public async Task WhenIAddThePhone()
        {
            var serializedPhoneNew = JsonConvert.SerializeObject(_phone);
            var stringContent = new StringContent(serializedPhoneNew, Encoding.UTF8, "application/json");
            _response = await TestHttpClient.PostAsync("api/phones", stringContent);
        }

        [Then(@"a phone is created in the system")]
        public void ThenAPhoneIsCreatedInTheSystem()
        {
            _response.StatusCode.Should().Be(HttpStatusCode.Created);
            _response.Headers.Location.AbsolutePath.Should().NotBeNullOrWhiteSpace();
        }
    }
}
