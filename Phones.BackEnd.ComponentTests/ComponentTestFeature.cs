﻿using Autofac;
using Phones.BackEnd.Host;
using System.Net.Http;
using ToolBelt.TestSupport;

namespace Phones.BackEnd.ComponentTests
{
    public class ComponentTestFeature
    {
        protected IComponentContext ComponentContext { get; }
        protected HttpClient TestHttpClient { get; }

        protected ComponentTestFeature()
        {
            var server = TestServerFactory<Startup>.Create();
            var services = server.Host.Services;
            ComponentContext = (IComponentContext)services.GetService(typeof(IComponentContext));
            TestHttpClient = server.CreateClient();
        }
    }
}
