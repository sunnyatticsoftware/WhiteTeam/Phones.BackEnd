﻿using Autofac;
using Phones.BackEnd.Application.Contracts.Factories;
using Phones.BackEnd.Application.Factories;

namespace Phones.BackEnd.Host.IoCC.Autofac.Modules
{
    public class FactoryModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<IdFactory>()
                .As<IIdFactory>()
                .SingleInstance();
        }
    }
}
