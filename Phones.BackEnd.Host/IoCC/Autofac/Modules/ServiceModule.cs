﻿using Autofac;
using Phones.BackEnd.Application.Contracts.Services;
using Phones.BackEnd.Application.Services;

namespace Phones.BackEnd.Host.IoCC.Autofac.Modules
{
    public class ServiceModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<PhoneReadService>()
                .As<IPhoneReadService>();

            builder
                .RegisterType<PhoneWriteService>()
                .As<IPhoneWriteService>();
        }
    }
}
