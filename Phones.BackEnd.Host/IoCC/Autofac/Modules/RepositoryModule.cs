﻿using Autofac;
using Phones.BackEnd.Application.Contracts.Repositories;
using Phones.BackEnd.Domain;
using Phones.BackEnd.Repository.Infra.EntityFramework;

namespace Phones.BackEnd.Host.IoCC.Autofac.Modules
{
    public class RepositoryModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<EntityFrameworkRepository>()
                .As<IRepository<PhoneAggregate>>();
        }
    }
}
