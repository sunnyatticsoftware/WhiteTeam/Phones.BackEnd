﻿using Autofac;
using Phones.BackEnd.Application.Contracts;
using Phones.BackEnd.Application.Mappers;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.Domain;
using Phones.BackEnd.Repository.Infra.EntityFramework.Entities;
using Phones.BackEnd.Repository.Infra.EntityFramework.Mappers;

namespace Phones.BackEnd.Host.IoCC.Autofac.Modules
{
    public class MapperModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<PhoneResponseMapper>()
                .As<IMapper<PhoneAggregate, PhoneResponse>>()
                .SingleInstance();

            builder
                .RegisterType<PhoneAggregateMapper>()
                .As<IMapper<PhoneEntity, PhoneAggregate>>()
                .SingleInstance();

            builder
                .RegisterType<PhoneEntityMapper>()
                .As<IMapper<PhoneAggregate, PhoneEntity>>()
                .SingleInstance();
        }
    }
}
