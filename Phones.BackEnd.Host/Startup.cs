﻿using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Phones.BackEnd.Host.Configuration;
using Phones.BackEnd.Repository.Infra.EntityFramework;
using Phones.BackEnd.WebApi.Controllers;
using Swashbuckle.AspNetCore.Swagger;

namespace Phones.BackEnd.Host
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services
                .AddDbContext<PhoneContext>(
                    options =>
                    {
                        options.UseInMemoryDatabase("PhonesDatabase");
                    });

            services
                .AddMvc()
                .AddApplicationPart(typeof(PhonesController).Assembly)
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddOptions();

            services.AddSwaggerGen(
                swagger =>
                {
                    var contact = new Contact
                    {
                        Name = SwaggerConfiguration.ContactName,
                        Url = SwaggerConfiguration.ContactUrl
                    };

                    var swaggerInfo =
                        new Info
                        {
                            Title = SwaggerConfiguration.DocInfoTitle,
                            Version = SwaggerConfiguration.DocInfoVersion,
                            Description = SwaggerConfiguration.DocInfoDescription,
                            Contact = contact
                        };

                    swagger.SwaggerDoc(SwaggerConfiguration.DocNameV1, swaggerInfo);
                });
            services.ConfigureSwaggerGen(options =>
            {
                options.CustomSchemaIds(x => x.FullName);
            });
        }

        public void ConfigureContainer(ContainerBuilder containerBuilder)
        {
            var hostAssembly = typeof(Startup).Assembly;
            containerBuilder.RegisterAssemblyModules(hostAssembly);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(SwaggerConfiguration.EndpointUrl, SwaggerConfiguration.EndpointDescription);
            });

            app.UseMvc();
        }
    }
}
