﻿namespace Phones.BackEnd.Host.Configuration
{
    public class SwaggerConfiguration
    {
        public const string EndpointDescription = "Swagger API v1";
        public const string EndpointUrl = "/swagger/v1/swagger.json";
        public const string ContactName = "TUI";
        public const string ContactUrl = "";
        public const string DocNameV1 = "v1";
        public const string DocInfoTitle = "Swagger API";
        public const string DocInfoVersion = "v1";
        public const string DocInfoDescription = "Swagger Api";
    }
}