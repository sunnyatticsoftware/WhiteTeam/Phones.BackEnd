﻿using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Types.UnitTests.BrandTypeTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private BrandType _sut;
            private BrandType _expectedValue;

            protected override void Given()
            {
                _sut = new BrandType();

                _expectedValue = BrandType.None;
            }

            protected override void When()
            {
                _sut = new BrandType();
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Have_None_Default_Value()
            {
                _sut.Should().Be(_expectedValue);
            }
        }
    }
}