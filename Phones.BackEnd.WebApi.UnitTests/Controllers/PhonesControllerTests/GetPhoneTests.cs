﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Phones.BackEnd.Application.Contracts.Services;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.WebApi.Controllers;
using System;
using System.Threading.Tasks;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.WebApi.UnitTests.Controllers.PhonesControllerTests
{
    public static class GetPhoneTests
    {
        public class Given_A_Phone_Id_When_Getting_Phone
            : Given_WhenAsync_Then_Test
        {
            private PhonesController _sut;
            private Mock<IPhoneReadService> _phoneReadRepositoryMock;
            private IActionResult _result;
            private Guid _id;

            protected override void Given()
            {
                var phoneWriteRepository = Mock.Of<IPhoneWriteService>();
                _phoneReadRepositoryMock = new Mock<IPhoneReadService>();

                _id = GuidGenerator.Create(1);
                var image = new Uri("http://one.jpg");
                var phone =
                    new PhoneResponse
                    {
                        Id = _id,
                        Title = "phone",
                        Description = "description",
                        ImageUri = image
                    };

                _phoneReadRepositoryMock
                    .Setup(x => x.GetPhone(_id))
                    .ReturnsAsync(phone);

                var phoneReadRepository = _phoneReadRepositoryMock.Object;

                _sut = new PhonesController(phoneWriteRepository, phoneReadRepository);
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.GetPhone(_id);
            }


            [Fact]
            public void Then_It_Should_Get_The_Phone_With_Repository()
            {
                _phoneReadRepositoryMock.Verify(x => x.GetPhone(_id), Times.Once);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.Should().BeAssignableTo<OkObjectResult>();
            }
        }
    }
}