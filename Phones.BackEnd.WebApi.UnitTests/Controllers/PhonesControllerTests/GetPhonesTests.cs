﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Phones.BackEnd.Application.Contracts.Services;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.WebApi.Controllers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.WebApi.UnitTests.Controllers.PhonesControllerTests
{
    public static class GetPhonesTests
    {
        public class Given_No_Query_Parameters_When_Getting_Phones
            : Given_WhenAsync_Then_Test
        {
            private PhonesController _sut;
            private Mock<IPhoneReadService> _phoneReadRepositoryMock;
            private IActionResult _result;

            protected override void Given()
            {
                var phoneWriteRepository = Mock.Of<IPhoneWriteService>();
                _phoneReadRepositoryMock = new Mock<IPhoneReadService>();

                var id1 = GuidGenerator.Create(1);
                var image1 = new Uri("http://one.jpg");
                var phoneOne =
                    new PhoneResponse
                    {
                        Id = id1,
                        Title = "phone1",
                        Description = "description1",
                        ImageUri = image1
                    };

                var id2 = GuidGenerator.Create(2);
                var image2 = new Uri("http://two.jpg");
                var phoneTwo =
                    new PhoneResponse
                    {
                        Id = id2,
                        Title = "phone2",
                        Description = "description2",
                        ImageUri = image2
                    };

                var phones =
                    new List<PhoneResponse>
                    {
                        phoneOne,
                        phoneTwo
                    };

                _phoneReadRepositoryMock
                    .Setup(x => x.GetPhones())
                    .ReturnsAsync(phones);

                var phoneReadRepository = _phoneReadRepositoryMock.Object;

                _sut = new PhonesController(phoneWriteRepository, phoneReadRepository);
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.GetPhones();
            }


            [Fact]
            public void Then_It_Should_Get_All_Phones_With_Repository()
            {
                _phoneReadRepositoryMock.Verify(x => x.GetPhones(), Times.Once);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.Should().BeAssignableTo<OkObjectResult>();
            }
        }
    }
}