﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Phones.BackEnd.Application.Contracts.Services;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.Types;
using Phones.BackEnd.WebApi.Controllers;
using System.Threading.Tasks;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.WebApi.UnitTests.Controllers.PhonesControllerTests
{
    public static class CreatePhoneTests
    {
        public class Given_A_PhoneNew_When_Creating
            : Given_WhenAsync_Then_Test
        {
            private PhonesController _sut;
            private Mock<IPhoneWriteService> _phoneWriteRepository;
            private IActionResult _result;
            private PhoneNew _phoneNew;

            protected override void Given()
            {
                _phoneWriteRepository = new Mock<IPhoneWriteService>();
                var phoneWriteRepository = _phoneWriteRepository.Object;
                var phoneReadRepository = Mock.Of<IPhoneReadService>();

                _phoneNew =
                    new PhoneNew
                    {
                        Brand = BrandType.Motorola,
                        Model = "foo",
                        Description = "bar"
                    };

                _sut = new PhonesController(phoneWriteRepository, phoneReadRepository);
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.CreatePhone(_phoneNew);
            }

            [Fact]
            public void Then_It_Should_Create_The_New_Phone_With_The_Repository()
            {
                _phoneWriteRepository.Verify(x => x.CreatePhone(_phoneNew), Times.Once);
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Response()
            {
                _result.Should().NotBeNull();
            }
        }
    }
}