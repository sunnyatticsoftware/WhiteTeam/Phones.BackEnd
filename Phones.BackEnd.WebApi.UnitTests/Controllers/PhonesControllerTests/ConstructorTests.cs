﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Phones.BackEnd.Application.Contracts.Services;
using Phones.BackEnd.WebApi.Controllers;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.WebApi.UnitTests.Controllers.PhonesControllerTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private PhonesController _sut;
            private IPhoneWriteService _phoneWriteService;
            private IPhoneReadService _phoneReadService;

            protected override void Given()
            {
                _phoneWriteService = Mock.Of<IPhoneWriteService>();
                _phoneReadService = Mock.Of<IPhoneReadService>();
            }

            protected override void When()
            {
                _sut = new PhonesController(_phoneWriteService, _phoneReadService);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_A_Controller()
            {
                _sut.Should().BeAssignableTo<Controller>();
            }
        }
    }
}