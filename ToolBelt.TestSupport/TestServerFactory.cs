﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using System;

namespace ToolBelt.TestSupport
{
    public static class TestServerFactory<TStartup>
        where TStartup : class
    {
        public static TestServer Create(Action<ContainerBuilder> containerBuilder = null)
        {
            var configuration =
                new ConfigurationBuilder()
                    .AddJsonFile("appsettings.Test.json")
                    .Build();



            var webHostBuilder =
                new WebHostBuilder()
                    .ConfigureServices(services => services.AddAutofac())
                    .ConfigureTestContainer(containerBuilder ?? DoNothing)
                    .UseConfiguration(configuration)
                    .UseStartup<TStartup>();

            var server = new TestServer(webHostBuilder);
            return server;
        }

        private static void DoNothing(ContainerBuilder containerBuilder)
        {
        }
    }
}