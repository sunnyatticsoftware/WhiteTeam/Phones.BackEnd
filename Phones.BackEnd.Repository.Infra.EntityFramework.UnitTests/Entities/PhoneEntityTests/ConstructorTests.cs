﻿using FluentAssertions;
using Phones.BackEnd.Repository.Infra.EntityFramework.Entities;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Repository.Infra.EntityFramework.UnitTests.Entities.PhoneEntityTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private PhoneEntity _sut;

            protected override void Given()
            {
            }

            protected override void When()
            {
                _sut = new PhoneEntity();
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }
        }
    }
}