﻿using Autofac;
using FluentAssertions;
using Phones.BackEnd.Application.Contracts.Factories;
using Phones.BackEnd.Application.Contracts.Repositories;
using Phones.BackEnd.Application.Contracts.Services;
using Phones.BackEnd.Application.Services;
using Phones.BackEnd.Domain;
using Phones.BackEnd.Repository.Infra.EntityFramework;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Host.IntegrationTests.IoCC.Autofac.ResolveTests
{
    public static class ResolveTests
    {
        public class Given_A_Startup_Initiated_When_Resolving_IIdFactory
            : Given_When_Then_Test
        {
            private IComponentContext _sut;
            private IIdFactory _result;

            protected override void Given()
            {
                var testServer = TestServerFactory<Startup>.Create();
                var services = testServer.Host.Services;
                _sut = (IComponentContext)services.GetService(typeof(IComponentContext));
            }

            protected override void When()
            {
                _result = _sut.Resolve<IIdFactory>();
            }

            [Fact]
            public void Then_It_Should_Resolve_The_IdFactory()
            {
                _result.Should().BeAssignableTo<IIdFactory>();
            }
        }

        public class Given_A_Startup_Initiated_When_Resolving_IPhoneWriteService
            : Given_When_Then_Test
        {
            private IComponentContext _sut;
            private IPhoneWriteService _result;

            protected override void Given()
            {
                var testServer = TestServerFactory<Startup>.Create();
                var services = testServer.Host.Services;
                _sut = (IComponentContext)services.GetService(typeof(IComponentContext));
            }

            protected override void When()
            {
                _result = _sut.Resolve<IPhoneWriteService>();
            }

            [Fact]
            public void Then_It_Should_Resolve_The_PhoneWriteService()
            {
                _result.Should().BeAssignableTo<PhoneWriteService>();
            }
        }

        public class Given_A_Startup_Initiated_When_Resolving_IPhoneReadService
            : Given_When_Then_Test
        {
            private IComponentContext _sut;
            private IPhoneReadService _result;

            protected override void Given()
            {
                var testServer = TestServerFactory<Startup>.Create();
                var services = testServer.Host.Services;
                _sut = (IComponentContext)services.GetService(typeof(IComponentContext));
            }

            protected override void When()
            {
                _result = _sut.Resolve<IPhoneReadService>();
            }

            [Fact]
            public void Then_It_Should_Resolve_The_PhoneReadService()
            {
                _result.Should().BeAssignableTo<PhoneReadService>();
            }
        }

        public class Given_A_Startup_Initiated_When_Resolving_IRepository_Of_PhoneAggregate
            : Given_When_Then_Test
        {
            private IComponentContext _sut;
            private IRepository<PhoneAggregate> _result;

            protected override void Given()
            {
                var testServer = TestServerFactory<Startup>.Create();
                var services = testServer.Host.Services;
                _sut = (IComponentContext)services.GetService(typeof(IComponentContext));
            }

            protected override void When()
            {
                _result = _sut.Resolve<IRepository<PhoneAggregate>>();
            }

            [Fact]
            public void Then_It_Should_Be_The_EntityFrameworkRepository()
            {
                _result.Should().BeAssignableTo<EntityFrameworkRepository>();
            }
        }
    }
}