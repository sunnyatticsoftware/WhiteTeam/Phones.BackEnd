﻿using FluentAssertions;
using Moq;
using Phones.BackEnd.Application.Contracts.Factories;
using Phones.BackEnd.Application.Contracts.Repositories;
using Phones.BackEnd.Application.Services;
using Phones.BackEnd.Application.UnitTests.TestSupport.Builders;
using Phones.BackEnd.Domain;
using System;
using System.Threading.Tasks;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Application.UnitTests.Services.PhoneWriteServiceTests
{
    public static class AddImageTests
    {
        public class Given_An_Image_Uri_When_Adding
            : Given_WhenAsync_Then_Test
        {
            private PhoneWriteService _sut;
            private Mock<IRepository<PhoneAggregate>> _repositoryMock;
            private Guid _id;
            private PhoneAggregate _phoneAggregate;
            private Uri _imageUri;

            protected override void Given()
            {
                var idFactory = Mock.Of<IIdFactory>();
                _repositoryMock = new Mock<IRepository<PhoneAggregate>>();
                _id = GuidGenerator.Create(1);
                _phoneAggregate =
                    new PhoneAggregateBuilder()
                        .WithId(_id)
                        .WithModel("model")
                        .Build();

                _imageUri = new Uri("http://foo.jpg");
                _repositoryMock
                    .Setup(x => x.Get(_id))
                    .ReturnsAsync(_phoneAggregate);
                var repository = _repositoryMock.Object;
                _sut = new PhoneWriteService(repository, idFactory);
            }

            protected override async Task WhenAsync()
            {
                await _sut.AddImage(_id, _imageUri);
            }

            [Fact]
            public void Then_It_Should_Retrieve_The_Phone_Aggregate_With_The_Repository()
            {
                _repositoryMock.Verify(x => x.Get(_id), Times.Once);
            }

            [Fact]
            public void Then_It_Should_Persist_The_Phone_Aggregate()
            {
                _repositoryMock.Verify(x => x.Persist(_phoneAggregate), Times.Once);
            }

            [Fact]
            public void Then_It_Should_Set_The_Correct_ImageUri()
            {
                _phoneAggregate.ImageUri.Should().Be(_imageUri);
            }
        }
    }
}