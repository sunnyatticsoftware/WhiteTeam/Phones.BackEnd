﻿using Moq;
using Phones.BackEnd.Application.Contracts.Factories;
using Phones.BackEnd.Application.Contracts.Repositories;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.Application.Services;
using Phones.BackEnd.Application.UnitTests.TestSupport.Builders;
using Phones.BackEnd.Domain;
using System.Threading.Tasks;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Application.UnitTests.Services.PhoneWriteServiceTests
{
    public static class CreatePhoneTests
    {
        public class Given_A_New_Phone_When_Creating
            : Given_WhenAsync_Then_Test
        {
            private PhoneWriteService _sut;
            private Mock<IRepository<PhoneAggregate>> _repositoryMock;
            private Mock<IIdFactory> _idFactoryMock;
            private PhoneNew _phoneNew;

            protected override void Given()
            {
                _repositoryMock = new Mock<IRepository<PhoneAggregate>>();
                var repository = _repositoryMock.Object;

                _phoneNew =
                    new PhoneNewBuilder()
                        .WithModel("model")
                        .Build();

                _idFactoryMock = new Mock<IIdFactory>();
                var id = GuidGenerator.Create(1);
                _idFactoryMock
                    .Setup(x => x.Create())
                    .Returns(id);
                var idFactory = _idFactoryMock.Object;
                _sut = new PhoneWriteService(repository, idFactory);
            }

            protected override async Task WhenAsync()
            {
                await _sut.CreatePhone(_phoneNew);
            }

            [Fact]
            public void Then_It_Should_Generate_New_Id()
            {
                _idFactoryMock.Verify(x => x.Create(), Times.Once);
            }

            [Fact]
            public void Then_It_Should_Persist_The_Phone_Aggregate()
            {
                _repositoryMock.Verify(x => x.Persist(It.IsAny<PhoneAggregate>()), Times.Once);
            }
        }
    }
}