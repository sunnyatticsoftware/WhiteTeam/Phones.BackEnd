﻿using FluentAssertions;
using Moq;
using Phones.BackEnd.Application.Contracts;
using Phones.BackEnd.Application.Contracts.Repositories;
using Phones.BackEnd.Application.Model;
using Phones.BackEnd.Application.Services;
using Phones.BackEnd.Domain;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Application.UnitTests.Services.PhoneReadServiceTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private PhoneReadService _sut;
            private IRepository<PhoneAggregate> _repository;
            private IMapper<PhoneAggregate, PhoneResponse> _phoneResponseMapper;

            protected override void Given()
            {
                _repository = Mock.Of<IRepository<PhoneAggregate>>();
                _phoneResponseMapper = Mock.Of<IMapper<PhoneAggregate, PhoneResponse>>();
            }

            protected override void When()
            {
                _sut = new PhoneReadService(_repository, _phoneResponseMapper);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }
        }
    }
}