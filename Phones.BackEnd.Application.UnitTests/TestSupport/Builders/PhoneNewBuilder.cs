﻿using Phones.BackEnd.Application.Model;
using Phones.BackEnd.Types;
using System;

namespace Phones.BackEnd.Application.UnitTests.TestSupport.Builders
{
    public class PhoneNewBuilder
    {
        private Guid _id;
        private BrandType _brand;
        private string _model;
        private string _description;

        public PhoneNewBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public PhoneNewBuilder WithBrand(BrandType brandType)
        {
            _brand = brandType;
            return this;
        }

        public PhoneNewBuilder WithModel(string model)
        {
            _model = model;
            return this;
        }

        public PhoneNewBuilder WithDescription(string description)
        {
            _description = description;
            return this;
        }

        public PhoneNew Build()
        {
            var phoneNew =
                new PhoneNew
                {
                    Id = _id,
                    Brand = _brand,
                    Description = _description,
                    Model = _model
                };
            return phoneNew;
        }
    }
}
