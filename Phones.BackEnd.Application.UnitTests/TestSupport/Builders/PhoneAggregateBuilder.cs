﻿using Phones.BackEnd.Domain;
using Phones.BackEnd.Types;
using System;

namespace Phones.BackEnd.Application.UnitTests.TestSupport.Builders
{
    public class PhoneAggregateBuilder
    {
        private Guid _id;
        private BrandType _brand;
        private string _model;
        private string _description;

        public PhoneAggregateBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public PhoneAggregateBuilder WithBrand(BrandType brandType)
        {
            _brand = brandType;
            return this;
        }

        public PhoneAggregateBuilder WithModel(string model)
        {
            _model = model;
            return this;
        }

        public PhoneAggregateBuilder WithDescription(string description)
        {
            _description = description;
            return this;
        }

        public PhoneAggregate Build()
        {
            var phoneAggregate = new PhoneAggregate(_id, _brand, _model, _description);
            return phoneAggregate;
        }
    }
}
