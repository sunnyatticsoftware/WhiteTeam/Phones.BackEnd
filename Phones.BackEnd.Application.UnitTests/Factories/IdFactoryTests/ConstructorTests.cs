﻿using FluentAssertions;
using Phones.BackEnd.Application.Contracts.Factories;
using Phones.BackEnd.Application.Factories;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Application.UnitTests.Factories.IdFactoryTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private IdFactory _sut;

            protected override void Given()
            {
            }

            protected override void When()
            {
                _sut = new IdFactory();
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IIdFactory()
            {
                _sut.Should().BeAssignableTo<IIdFactory>();
            }
        }
    }
}