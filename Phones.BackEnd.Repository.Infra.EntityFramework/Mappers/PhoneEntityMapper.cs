﻿using Phones.BackEnd.Application.Contracts;
using Phones.BackEnd.Domain;
using Phones.BackEnd.Repository.Infra.EntityFramework.Entities;

namespace Phones.BackEnd.Repository.Infra.EntityFramework.Mappers
{
    public class PhoneEntityMapper
        : IMapper<PhoneAggregate, PhoneEntity>
    {
        public PhoneEntity Map(PhoneAggregate source)
        {
            var id = source.Id;
            var brand = source.Brand;
            var model = source.Model;
            var description = source.Description;
            var imageUri = source.ImageUri?.AbsoluteUri;
            var phoneEntity =
                new PhoneEntity
                {
                    Id = id,
                    Brand = brand,
                    Model = model,
                    Description = description,
                    ImageUri = imageUri
                };
            return phoneEntity;
        }
    }
}
