﻿using Phones.BackEnd.Application.Contracts;
using Phones.BackEnd.Domain;
using Phones.BackEnd.Repository.Infra.EntityFramework.Entities;
using System;

namespace Phones.BackEnd.Repository.Infra.EntityFramework.Mappers
{
    public class PhoneAggregateMapper
        : IMapper<PhoneEntity, PhoneAggregate>
    {
        public PhoneAggregate Map(PhoneEntity source)
        {
            var id = source.Id;
            var brand = source.Brand;
            var model = source.Model;
            var description = source.Description;
            var imageUriText = source.ImageUri;
            var phoneAggregate = new PhoneAggregate(id, brand, model, description);
            if (!string.IsNullOrWhiteSpace(imageUriText))
            {
                var imageUri = new Uri(imageUriText);
                phoneAggregate.AddImageUri(imageUri);
            }
            return phoneAggregate;
        }
    }
}
