﻿using Phones.BackEnd.Types;
using System;

namespace Phones.BackEnd.Repository.Infra.EntityFramework.Entities
{
    public class PhoneEntity
    {
        public Guid Id { get; set; }
        public BrandType Brand { get; set; }
        public string Model { get; set; }
        public string Description { get; set; }
        public string ImageUri { get; set; }
    }
}
