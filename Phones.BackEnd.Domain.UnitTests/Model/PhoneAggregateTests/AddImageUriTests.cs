﻿using System;
using FluentAssertions;
using Phones.BackEnd.Domain.Exceptions;
using Phones.BackEnd.Types;
using ToolBelt.TestSupport;
using Xunit;

namespace Phones.BackEnd.Domain.UnitTests.Model.PhoneAggregateTests
{
    public static class AddImageUriTests
    {
        public class Given_A_Phone_Without_An_ImageUri_When_Adding_An_ImageUri
            : Given_When_Then_Test
        {
            private PhoneAggregate _sut;
            private Uri _imageUri;
            private Exception _exception;

            protected override void Given()
            {
                _imageUri = new Uri("http://test.jpg");

                var id = GuidGenerator.Create(1);
                var brand = BrandType.Motorola;
                var model = "foo";
                var description = "bar";

                _sut = new PhoneAggregate(id, brand, model, description);
            }

            protected override void When()
            {
                try
                {
                    _sut.AddImageUri(_imageUri);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Not_Throw_Any_Exception()
            {
                _exception.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Have_The_Correct_Image()
            {
                _sut.ImageUri.Should().Be(_imageUri);
            }
        }

        public class Given_A_Phone_With_An_Already_Existing_ImageUri_When_Adding_An_ImageUri
            : Given_When_Then_Test
        {
            private PhoneAggregate _sut;
            private Uri _newImageUri;
            private Uri _expectedImageUri;
            private ImageAlreadyExistsException _exception;

            protected override void Given()
            {
                _newImageUri = new Uri("http://newtest.jpg");

                var id = GuidGenerator.Create(1);
                var brand = BrandType.Motorola;
                var model = "foo";
                var description = "bar";
                var imageUri = new Uri("http://test.jpg");

                _sut = new PhoneAggregate(id, brand, model, description);
                _sut.AddImageUri(imageUri);

                _expectedImageUri = imageUri;
            }

            protected override void When()
            {
                try
                {
                    _sut.AddImageUri(_newImageUri);
                }
                catch (ImageAlreadyExistsException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Throw_An_ImageAlreadyExistsException()
            {
                _exception.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Have_The_Correct_Existing_Image()
            {
                _sut.ImageUri.Should().Be(_expectedImageUri);
            }
        }
    }
}