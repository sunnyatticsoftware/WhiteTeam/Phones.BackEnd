# Phones.BackEnd

## Descripcion

Phones.BackEnd es una aplicacion realizada en C# con tecnologias como AspNetCore, AspNetCore Mvc, EntityFrameworkCore, Autofac, Swagger, xUnit, Moq, FluentAssertions, MvcTesting y SpecFlow.

Se trata de una API http servida por Kestrel donde se pueden crear telefonos, actualizar su imagen, recuperar el listado de todos los que hay o de alguno en concreto.
La persistencia es en memoria utilizando EntityFramework InMemoryDatabase

## Como Ejecutar

El unico requisito necesario para ejecutar la aplicacion es disponer del SDK de AspNetCore 2.2 instalado

1. Clonar el repositorio desde una consola ejecutando `git clone https://gitlab.com/WhiteTeam/Phones.BackEnd.git`
2. Dentro del directorio `Phones.BackEnd` que se crea, compilar la solucion ejecutando `dotnet build`
3. Dentro del directorio `Phones.BackEnd/Phones.BackEnd.Host` ejecutar `dotnet run` para lanzar la aplicacion con kestrel. La aplicacion estara lista en `http://localhost:50000`
4. Abrir la URL http://localhost:50000/swagger/index.html para ver todos los endpoints y los verbos http soportados y para hacer pruebas manuales

## Buenas Practicas Empleadas

### Test-Driven Development (TDD)

En general, la aplicacion se ha desarrollado utilizando TDD. Primero se han implementado los tests, incluso aquellos para instanciar determinada clase, mockeando las dependencias, y despues se ha implementado la logica

### Behavior-Driven Development (BDD)

Aunque en esta ocasion el desarrollo se ha hecho con TDD, se han incluido tests de componentes con SpecFlow para demostrar como se pueden hacer tests automatizados legibles para requisitos funcionales de negocio (con lenguaje gherkin)
que se redactan (*.feature) y, gracias a SpecFlow, se genera despues una serie de pasos a programar para satisfacer los escenarios planteados.

### Continuous Integration / Continuous Deployment (CI/CD)

Para este ejemplo se ha utilizado la pipeline CI/CD de GitLab. Este pipeline se activa cada vez que nuevo codigo es a�adido a la rama `master`. Concretamente se ha dejado de lado la parte de despliegue por no disponer de tiempo
pero para la parte de integracion continua se ha configurado de modo que GitLab compile y ejecute automaticamente todos los test unitarios, de integracion y de componentes.

### SOLID

Se han empleado principios SOLID en el desarrollo y en la arquitectura de la solucion. Se ha utilizado Autofac para la inyeccion de dependencias y el codigo respeta el principio de responsabilidad unica ademas de otros.
Ademas, se utilizan diferentes modelos en diferentes capas, de modo que los cambios en el modelo que se expone al exterior no afecte a la logica de dominio y viceversa.

### Domain Driven Design (DDD)

A pesar de que el ejemplo es sencillo y el modelo de dominio muy simple, se ha optado por emplear tecnicas de implementacion de Domain Driven Design y utilizar un agregado con estado + comportamiento en lugar de un modelo anemico.
Se ha comenzado a implementar la aplicacion por su capa de dominio, que es la que contiene la logica de negocio y la que refleja el lenguaje ubicuo, y no por su modelo relacional o de persistencia. Esto permite aislar completamente
los requisitos de negocio de la forma en la que la informacion se persiste, y abre la posibilidad a cambiar facilmente tecnologias y frameworks sin afectar a las capas de dominio o aplicacion y sin romper sus tests.

### Clean Architecture

Se ha hecho especial hincapie en la separacion de capas empleando una arquitectura limpia donde la capa de dominio es la principal y la mas interna. La capa de aplicacion actua como orquestadora de la logica de negocio. Finalmente
las capas de infraestructura son las externas y son totalmente reemplazables. El grafico de dependencias quedaria como muestra la imagen:
![alt text](dependencyDiagram.png)

Las capas de dominio y aplicacion no dependen de ningun framework externo que no se incluya en el estandar de .NET, ni de ninguna tecnologia especifica. Esto permite gran modularidad y la posibilidad de cambiar las capas externas de
infraestructura (persistencia en base de datos, protocolo de comunicacion con el exterior, serializado/deserializado, etc.) sin afectar lo mas minimo a la logica de negocio y de orquestacion.

### Test automatizados

Como se ha explicado anteriormente, la solucion cuenta con pruebas unitarias y de integracion incluyendo pruebas de componente donde la entrada es la peticion http y la validacion se hace contra su respuesta.

### Swagger

Se ha incluido un middleware de swagger que muestra la documentacion de la API web y permite realizar pruebas manuales con peticiones http.


## Cosas a Mejorar

1. Si hubiese requisitos de negocio mas complejos, se optaria por una persistencia en disco con una base de datos relacional, por ejemplo.
2. Si el modelo de dominio fuese mas complejo, se optaria por una arquitectura CQRS y, posiblemente, con Event Sourcing para separar completamente los canales de escritura y de lectura. El modelo de dominio seria solamente de escritura
y contendria toda la logica de negocio de modo que almacenaria todo cambio en el sistema en base a eventos que podrian servir como historial o para incluir funcionalidad futura de minado de datos. El modelo de lectura se actualizaria 
proyectando los eventos que se han producido en diferentes proyecciones (vistas) para optimizar la velocidad de lectura y hacer que los clientes http puedan consumir lo que necesitan sin transformar el modelo. De esta forma no se
necesitaria en absoluto costosas bases de datos relacionales y se abriria la puerta a una mayor escalabilidad horizontal en una arquitectura distribuida basada en eventos y asincrona.
